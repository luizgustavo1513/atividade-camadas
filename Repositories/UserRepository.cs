﻿using Entities;

namespace Repositories
{
    public interface IUserRepository
    {
        void create(User user);
        User? findByEmail(String email);
        User? findById(int id);
        List<User> findAll();
        void update(User user);
        void delete(User user);
    }

    public class UserRepository : IUserRepository
    {
        private DataContext _dataContext;

        public UserRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void create(User user)
        {
            _dataContext.Add(user);

            _dataContext.SaveChanges();
        }

        public User? findByEmail(String email)
        {
            var user = _dataContext.User.Where(p => p.email == email).FirstOrDefault();

            return user;
        }

        public User? findById(int id)
        {
            var user = _dataContext.User.Where(p => p.id == id).FirstOrDefault();

            return user;
        }

        public List<User> findAll()
        {
            var list = _dataContext.User.ToList();

            return list;
        }

        public void update(User user)
        {
            var userOnDb = _dataContext.User.Where(p=>p.id ==user.id).First();

            userOnDb.name = user.name;
            userOnDb.email = user.email;
            userOnDb.password = user.password;
            userOnDb.image = user.image;

            _dataContext.SaveChanges();
        }

        public void delete(User user)
        {
            _dataContext.Remove(user);
            _dataContext.SaveChanges();
        }
    }
}
