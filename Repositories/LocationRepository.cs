﻿using Entities;

namespace Repositories
{
    public interface ILocationRepository
    {
        void create(Location location);

        Location? findById(int id);
        List<Location> list();
        void update(Location location);
        void delete(Location location);

    }
    public class LocationRepository : ILocationRepository
    {
        private DataContext _dataContext;

        public LocationRepository(DataContext dataContext) 
        {
            _dataContext = dataContext;
        }

        public void create(Location location)
        {
            _dataContext.Add(location);

            _dataContext.SaveChanges();
        }

        public List<Location> list()
        {
            var list = _dataContext.Location.ToList();

            return list;
        }

        public Location? findById(int id)
        {
            var location = _dataContext.Location.Where(p=>p.id ==id).FirstOrDefault();

            return location;
        }

        public void update( Location location)
        {
            var locationOnDb = _dataContext.Location.Where(p => p.id == location.id).First();

            locationOnDb.name = location.name;
            locationOnDb.city = location.city;
            locationOnDb.address = location.address;
            locationOnDb.document = location.document;
            locationOnDb.phoneNumber = location.phoneNumber;
            locationOnDb.description = location.description;
            locationOnDb.bannerImage = location.bannerImage;

            _dataContext.SaveChanges();

        }

        public void delete(Location location)
        {
            _dataContext.Remove(location);

            _dataContext.SaveChanges();
        }


    }
}
