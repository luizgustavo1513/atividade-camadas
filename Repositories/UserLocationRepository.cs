﻿using Entities;
using Microsoft.EntityFrameworkCore;

namespace Repositories
{   
    public interface IUserLocationRepository
    {
        void create(UserLocation userLocation);
        void update(UserLocation userLocation);
        List<UserLocation> findAll();
        void delete(UserLocation userLocation);
        UserLocation? findById(int id);
    }
    public class UserLocationRepository:IUserLocationRepository
    {
        private DataContext _dbContext;
        public UserLocationRepository(DataContext dbContext)
        {
                _dbContext = dbContext;
        }

        public void create(UserLocation userLocation)
        {
           _dbContext.Add(userLocation);

            _dbContext.SaveChanges();
        }

        public void delete(UserLocation userLocation)
        {
            _dbContext.Remove(userLocation);
            _dbContext.SaveChanges();
        }

        public List<UserLocation> findAll()
        {
            var list = _dbContext.UserLocation.ToList();

            return list;
        }

        public UserLocation? findById(int id)
        {
            var userLocation = _dbContext.UserLocation.Where(p => p.id == id).FirstOrDefault();

            return userLocation;
        }

        public void update(UserLocation userLocation)
        {
            var userLocationOnDb = _dbContext.UserLocation.Where(p => p.id == userLocation.id).First();

            userLocationOnDb.type = userLocation.type;

            _dbContext.SaveChanges();
        }
    }
}
