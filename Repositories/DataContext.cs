﻿using Entities;
using Microsoft.EntityFrameworkCore;

namespace Repositories
{
    public class DataContext : DbContext
    {
        public DataContext() 
        {
        
        }

        public DataContext(DbContextOptions<DataContext> options) : base(options) 
        {

        }
        public DbSet<User> User { get; set; }
        public DbSet<Location> Location { get; set; }
        public DbSet<UserLocation> UserLocation { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasKey(p=>p.id);
            modelBuilder.Entity<Location>().HasKey(p=>p.id);
            modelBuilder.Entity<UserLocation>().HasKey(p=>p.id);

            modelBuilder.Entity<UserLocation>().HasOne( p => p.location).WithMany().HasForeignKey(p => p.locationId);
            modelBuilder.Entity<UserLocation>().HasOne( p => p.user).WithMany().HasForeignKey(p => p.userId);


            base.OnModelCreating(modelBuilder);
        }
    }
}
