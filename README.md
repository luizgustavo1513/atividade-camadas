<div align="center">
    
<h1>HS Agendamentos API</h1>

</div>

<h2>Propósito</h2>

Foi construido uma API para a criação de Salões de Beleza para resolver o problema incial em agendamentos. A API consta incialmente com a criação de Salões, Usuarios e o relacionamento entre eles

## Usuários do sistema
- Profissionais e Clientes da aréa de Salões de Beleza

<h2>  Requisitos funcionais </h2>
<div>
<h3>Usuários</h3>

- Caso de Uso: Login
    Descrição: Para acessar a API é necessário estar logado. Incialmente o email é 'admin@admin.com' e a senha 'admin'

- Caso de uso: Cadastro
    Descrição: Permitir cadastrar usuários com as seguintes informações:

    {

    "name": "string",

    "email": "string",// Único

    "image": "string",

    "password": "string"// Criptografada

    }

- Caso de uso: Atualização
    Descrição: Permitir atualizar usuários com as seguintes informações:

    {

    "name": "string",

    "email": "string",// Único

    "image": "string"

    }
    
- Caso de uso: Listagem
    Descrição: Permitir listar usuários com as seguintes informações:

    {

    "id": "int",

    "name": "string",

    "email": "string",

    "image": "string"

    }
  
- Caso de uso: Verificar email
    Descrição: Permitir verificar se o email ja foi cadastrado na base de dados:

    {

    "email": "string",// Único

    }

- Caso de uso: Deletar
    Descrição: Permitir deletar o usuário:

    {

    "id": "int",

    }

 </div> 
 <div>
<h3>Local</h3>

- Caso de uso: Cadastro
    Descrição: Permitir cadastrar locais com as seguintes informações:

    {

    "name": "string",

    "description": "string",

    "city": "string",

    "phoneNumber": "string",

    "address": "string",

    "document": "string",

    "bannerImage": "string"

    }

- Caso de uso: Atualização
    Descrição: Permitir atualizar os locais com as seguintes informações:

    {

    "id": "id",

    "name": "string",

    "description": "string",

    "city": "string",

    "phoneNumber": "string",

    "address": "string",

    "document": "string",

    "bannerImage": "string"

    }

- Caso de uso: Listagem
    Descrição: Permitir listar os locais com as seguintes informações:

    {

    "id": "int",

    "name": "string",

    "description": "string",

    "city": "string",

    "phoneNumber": "string",

    "address": "string",

    "document": "string",

    "bannerImage": "string",

    }

- Caso de uso: Deletar
    Descrição: Permitir deletar o Local:

    {

    "id": "int",

    }

</div>
<div>
<h3>Relacionamento de Local com Usuário</h3>

- Caso de uso: Cadastro
    Descrição: Permitir cadastrar locais com as seguintes informações:

    {

    "userId": "int",

    "locationId": "int",

    "type": "Professional" | "Client"

    }

- Caso de uso: Atualização
    Descrição: Permitir atualizar a categoria do relacionamento:

    {

    "id": "int",
    
    "type": "Professional" | "Client"

    }

- Caso de uso: Listagem
    Descrição: Permitir listar os relacionados:

    {

    "id": "int",

    "locationId": "int",

    "userId": "int",

    "type": "Professional" | "Client"

    }

- Caso de uso: Deletar
    Descrição: Permitir deletar o relacionamento:

    {

    "id": "int",
    
    }

</div>

# Time
<table>
  <tbody>
    <tr>
      <td align="center" valign="top" width="14.28%"><a href="https://gitlab.com/luizgustavo1513"><img src="https://secure.gravatar.com/avatar/ae7968fe7eb8d9d6bdd0379ad1752f9ba3dce33e96a681162f952077d38a3604?s=384&d=identicon" width="100px;" alt="Luiz Gustavo"/><br /><sub><b>Luiz Gustavo Peruchi de Oliveira</b></sub></a><br />
      <td align="center" valign="top" width="14.28%"><a href="https://github.com/VPente"><img src="https://avatars.githubusercontent.com/u/101226506?v=4" width="100px;" alt="Vitor Penteado"/><br /><sub><b>Vítor Penteado</b></sub></a><br /></td>
    </tr>
   
   
