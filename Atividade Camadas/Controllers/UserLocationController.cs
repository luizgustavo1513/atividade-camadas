﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services;

namespace Atividade_Camadas.Controllers
{
    [Authorize]
    [Route("/user/location")]
    [ApiController]
    public class UserLocationController : ControllerBase
    {
        private IUserLocationServices _services;
        public UserLocationController(IUserLocationServices services)
        {
            _services = services;
        }
        [Route("create")]
        [HttpPost]
        public ActionResult create(createUserLocationDTO userLocationDTO)
        {
            try
            {
                _services.create(userLocationDTO);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        [Route("list")]
        [HttpGet]
        public ActionResult list()
        {
            try
            {
               var list = _services.list();

                return Ok(list);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("update")]
        [HttpPut]
        public ActionResult update(updateUserLocationDTO userLocationDTO)
        {
            try
            {
              _services.update(userLocationDTO); 

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("delete")]
        [HttpDelete]
        public ActionResult delete(int id)
        {
            try
            {
              _services.delete(id);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

    }
}

