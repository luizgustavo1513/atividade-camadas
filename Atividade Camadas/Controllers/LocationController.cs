﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services;

namespace Atividade_Camadas
{
    [Authorize]
    [ApiController]
    [Route("/location")]
    public class LocationController : ControllerBase
    {
        private ILocationServices _services;
        public LocationController(ILocationServices services) 
        { 
            _services = services;
        }

        [Route("create")]
        [HttpPost]
        public ActionResult create(createLocationDTO location)
        {
            try
            {
                _services.create(location);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        [Route("list")]
        [HttpGet]
        public ActionResult list()
        {
            try
            {
                var list = _services.list();

                return Ok(list);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("update")]
        [HttpPut]
        public ActionResult update(int id,updateLocationDTO location)
        {
            try
            {
                _services.update(id,location);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("delete")]
        [HttpDelete]
        public ActionResult delete(int id)
        {
            try
            {
                _services.delete(id);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

    }
}

