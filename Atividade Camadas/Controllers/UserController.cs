using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services;
using System.Security.Claims;

namespace Atividade_Camadas
{
    [ApiController]
    [Route("/user")]
    public class UserController : ControllerBase
    {
        private IUserService _userService;

        public UserController(IUserService userService,IJWTService jwtService)
        {
            _userService = userService;
        }

        [Route("login")]
        [HttpPost]
        public IActionResult Login(loginUserDTO login)
        {
            try
            {
                var token = _userService.login(login);

                return Ok(new { token });

            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Authorize]
        [Route("create")]
        [HttpPost]
        public ActionResult create(createUserDTO user)
        {
            try
            {
                _userService.create(user);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        [Authorize]
        [Route("find")]
        [HttpGet]
        public ActionResult findUserByEmail(string email)
        {
            try
            {
                var user = _userService.findUserByEmail(email);

                return Ok(user);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Authorize]
        [Route("list")]
        [HttpGet]
        public ActionResult list()
        {
            try
            {
                var list = _userService.findAll();

                return Ok(list);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Authorize]
        [Route("update")]
        [HttpPut]
        public ActionResult update(updateUserDTO user)
        {
            try
            {
                _userService.update(user);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Authorize]
        [Route("delete")]
        [HttpDelete]
        public ActionResult delete(int id)
        {
            try
            {
                _userService.delete(id);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
            
    }
}
