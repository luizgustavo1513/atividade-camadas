﻿using Entities;
using Repositories;

namespace Services
{
    public interface ILocationServices
    {
        void create(createLocationDTO createLocation);
        void update(int id,updateLocationDTO updateLocation);
        List<Location> list();
        void delete(int id);
        Location verifyExistingLocation(int id);
    }
    public class LocationServices : ILocationServices
    {
        private ILocationRepository _repository;
        public LocationServices(ILocationRepository repository)
        {
            _repository = repository;
        }

        public void create(createLocationDTO createLocation) 
        {
            var location = new Location();

            location.name = createLocation.name;
            location.city = createLocation.city;
            location.address = createLocation.address;
            location.document = createLocation.document;
            location.bannerImage = createLocation.bannerImage;
            location.phoneNumber = createLocation.phoneNumber;
            location.description = createLocation.description;

            _repository.create(location);
        }

        public List<Location> list()
        {
            var list = _repository.list();

            return list;
        }

        public void update(int id, updateLocationDTO updateLocation)
        {
            verifyExistingLocation(id);

            var location = new Location();

            location.id = id;
            location.name = updateLocation.name;
            location.city = updateLocation.city;
            location.address = updateLocation.address;
            location.document = updateLocation.document;
            location.phoneNumber = updateLocation.phoneNumber;
            location.description = updateLocation.description;
            location.bannerImage = updateLocation.bannerImage;

            _repository.update(location);
        }

        public void delete(int id)
        {
            var location = verifyExistingLocation(id);

            _repository.delete(location);
        }

        public Location verifyExistingLocation(int id) {

            var location = _repository.findById(id);

            if (location == null)
            {
                throw new Exception("Local informado não consta na base de dados");
            }

            return location;
        }
    }
}
