﻿

using Entities;
using Repositories;

namespace Services
{
    public interface IUserLocationServices
    {
        void create(createUserLocationDTO createUserLocation);
        List<UserLocation> list();
        void delete(int id);
        void update(updateUserLocationDTO updateUserLocation);
    }
    public class UserLocationServices: IUserLocationServices
    {
        private IUserLocationRepository _repository;
        private IUserService _userService;
        private ILocationServices _locationServices;
        public UserLocationServices(IUserLocationRepository repository, IUserService userService, ILocationServices locationServices)
        {
            _repository = repository;
            _userService = userService;
            _locationServices = locationServices;

        }
        public void create(createUserLocationDTO createUserLocation)
        {
            var user = _userService.findUserById(createUserLocation.userId);
            if (user == null)
            {
                throw new Exception("Usuário informado não consta na base de dados");
            }
            _locationServices.verifyExistingLocation(createUserLocation.locationId);

            var userLocation = new UserLocation();

            userLocation.locationId = createUserLocation.locationId;
            userLocation.userId = createUserLocation.userId;
            userLocation.type = createUserLocation.type;
            _repository.create(userLocation);


        }

        public List<UserLocation> list()
        {
            var list = _repository.findAll();

            return list;
        }

        public void update(updateUserLocationDTO updateUserLocation)
        {
            verifyExistingUserLocation(updateUserLocation.id);

            var userLocation = new UserLocation();

            userLocation.id = updateUserLocation.id;

            userLocation.type = updateUserLocation.type;

            _repository.update(userLocation);
        }

        public void delete(int id)
        {
            var userLocation = verifyExistingUserLocation(id);

           _repository.delete(userLocation);
        }

        private UserLocation verifyExistingUserLocation(int id)
        {

            var userLocation = _repository.findById(id);

            if (userLocation == null)
            {
                throw new Exception("Relacionamento informado não consta na base de dados");
            }

            return userLocation;
        }
    }
}
