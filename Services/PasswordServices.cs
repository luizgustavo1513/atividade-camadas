﻿using System.Security.Cryptography;

namespace Services
{
    public interface IPasswordServices
    {
        string hashPassword(string password);
        bool verifyPassword(string passwordHash, string inputPassword);
    }
    public class PasswordServices : IPasswordServices
    {
        private const int SaltSize = 32;
        private const int KeySize = 64;
        private const int Iterations = 100000;
        private static readonly HashAlgorithmName _hashAlgorithmName = HashAlgorithmName.SHA256;
        private const char Delimiter = ';';

        public string hashPassword(string password)
        {
            var salt = RandomNumberGenerator.GetBytes(SaltSize);
            var hash = Rfc2898DeriveBytes.Pbkdf2(password, salt,Iterations,_hashAlgorithmName,KeySize);

            return string.Join(Delimiter, Convert.ToBase64String(salt),Convert.ToBase64String(hash));
        }

        public bool verifyPassword(string passwordHash, string inputPassword)
        {
            var elements = passwordHash.Split(Delimiter);
            var salt = Convert.FromBase64String(elements[0]);
            var hash = Convert.FromBase64String(elements[1]);

            var hashInput = Rfc2898DeriveBytes.Pbkdf2(inputPassword, salt, Iterations, _hashAlgorithmName, KeySize);

            return CryptographicOperations.FixedTimeEquals(hash, hashInput);
        }
    }
}
