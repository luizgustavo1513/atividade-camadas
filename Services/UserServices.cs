﻿using Entities;
using Repositories;
using System.Security.Claims;

namespace Services
{
    public interface IUserService
    {
        void create(createUserDTO user);
        findUserDTO findUserByEmail(string email);
        List<findUserDTO> findAll();
        void update(updateUserDTO user);
        void delete(int id);
        string login(loginUserDTO login);
        User findUserById(int id);

    }
    public class UserServices : IUserService
    {
        private IUserRepository _userRepository;
        private IPasswordServices _passwordServices;
        private IJWTService _jwtService;


        public UserServices(IUserRepository userRepository, IPasswordServices passwordServices, IJWTService jwtService)
        {
            _userRepository = userRepository;
            _passwordServices = passwordServices;
            _jwtService = jwtService;
        }

        public string login(loginUserDTO login)
        {
            var user = findAllUserDataByEmail(login.email);

            if (user == null)
            {
                throw new Exception("Usuário ou senha inválidos");
            }

            var validPassword = _passwordServices.verifyPassword(user.password, login.password);

            if(validPassword == false)
            {
                throw new Exception("Usuário ou senha inválidos");
            }

            var claims = new[] {
                   new Claim("name",user.name)
            };

            var token = _jwtService.generateToken(claims);

            return token;
        }

        public void create(createUserDTO user)
        {
            var existingUserEmail = findAllUserDataByEmail(user.email);

            if (existingUserEmail != null)
            {
                throw new Exception("Email informado já está cadastrado");
            }

            var completeUser = new User();

            completeUser.name = user.name;
            completeUser.email = user.email;
            completeUser.password = _passwordServices.hashPassword(user.password);
            completeUser.image = user.image;

            _userRepository.create(completeUser);
        }

        public findUserDTO findUserByEmail(string email)
        {
            var user = _userRepository.findByEmail(email);

            if (user == null)
            {
                throw new Exception("Email informado não consta na base de dados");
            }

            var foundUserDTO = new findUserDTO();

            foundUserDTO.name = user.name; foundUserDTO.email = user.email; foundUserDTO.image = user.image; foundUserDTO.id = user.id;

            return foundUserDTO;
        }

        public List<findUserDTO> findAll()
        {
            var list = _userRepository.findAll();

            var listFilter = list.Select(p=>new findUserDTO
            {
                id = p.id,
                name = p.name,
                email = p.email,
                image = p.image,
            }).ToList();

            return listFilter;
        }

        public void update(updateUserDTO user)
        {
            var existingUserEmail = findUserById(user.id);

            if (existingUserEmail != null && existingUserEmail.id != user.id)
            {
                throw new Exception("Usuário informado não consta na base de dados");
            }

            var completeUser = new User();
            completeUser.name = user.name;
            completeUser.email = user.email;
            completeUser.image = user.image;
            completeUser.id = user.id;

            _userRepository.update(completeUser);
        }

        public void delete(int id)
        {
            var existingUserEmail = findUserById(id);

            if (existingUserEmail == null)
            {
                throw new Exception("Usuário informado não consta na base de dados");
            }

            var user = _userRepository.findById(id);

            _userRepository.delete(user);
        }

        private User findAllUserDataByEmail(string email)
        {
            var user = _userRepository.findByEmail(email);

            return user;
        }

        public User findUserById(int id) 
        {
            var user = _userRepository.findById(id);

            return user;
        }
       
    }
}
