﻿using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Services
{
    public interface  IJWTService
    {
        string generateToken(Claim[] claims);
    }

    public class JWTService : IJWTService
    {
        public JWTService() { }

        public string generateToken(Claim[] claims)
        {
            string secretKey = "ef685a21-41f4-464e-8d3b-887ed9671924";

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey));
            var credential = new SigningCredentials(key,SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                issuer: "HS_AGENDAMENTOS",
                audience: "HS_AGENDAMENTOS_API",
                claims, // Nessa propriedade eu sei que posso passar quaisquer dados que eu queira para manipular depois nas rotas
                expires: DateTime.Now.AddHours(1),
                signingCredentials: credential
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

    }
}
