﻿
using Entities;

namespace Services
{
    public class updateUserLocationDTO
    {
        public int id {  get; set; }
        public UserLocationType type { get; set; }

    }
}
