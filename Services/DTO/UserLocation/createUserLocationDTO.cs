﻿
using Entities;

namespace Services
{
    public class createUserLocationDTO
    {
        public int userId {  get; set; }
        public int locationId { get; set; }
        public UserLocationType type { get; set; }

    }
}
