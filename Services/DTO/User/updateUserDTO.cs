﻿namespace Services
{
    public class updateUserDTO
    {
        public int id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string image { get; set; }

    }
}
