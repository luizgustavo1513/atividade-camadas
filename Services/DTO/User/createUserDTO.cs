﻿namespace Services
{
    public class createUserDTO
    {

        public string name { get; set; }
        public string email { get; set; }
        public string image { get; set; }
        public string password { get; set; }
    }
}
