﻿namespace Services
{
    public class findUserDTO
    {
        public int id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string image { get; set; }

    }
}
