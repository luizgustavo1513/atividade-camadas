﻿namespace Entities
{
    public class User
    {
        public int id {  get; set; }

        public string name { get; set; }

        public string email { get; set; }

        public string image { get; set; }

        public string password { get; set; }

        public DateTime createdAt { get; set; }

        public DateTime updatedAt { get; set; }

    }
}
