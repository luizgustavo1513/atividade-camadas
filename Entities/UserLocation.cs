﻿

namespace Entities
{
    public class UserLocation
    {
        public int id { get; set; }
        public int locationId { get; set; }
        public int userId { get; set; }
        public UserLocationType type { get; set; }
        public User user { get; set; }
        public Location location { get; set; }
    }
    public enum UserLocationType
    {
        Professional = 1,
            Client = 2,
    }
}
