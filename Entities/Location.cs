﻿namespace Entities
{
    public class Location
    {
        public int id {  get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string city { get; set; }
        public string phoneNumber { get; set; }
        public string address { get; set; }
        public string document {  get; set; }
        public string bannerImage { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
    }
}
